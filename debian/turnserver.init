#!/bin/sh
### BEGIN INIT INFO
# Provides:          turnserver
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: TurnServer.org TURN Server
# Description:       STUN and TURN Relay for VoIP media streams
### END INIT INFO

set -e

DAEMON=/usr/sbin/turnserver 
NAME=turnserver
CONFIG=/etc/turnserver/turnserver.conf
USER=turnserver
GROUP=turnserver

test -x $DAEMON || exit 0

PIDFILE="/var/run/$NAME/$NAME.pid"
PIDFILE_DIR=`dirname $PIDFILE`
umask 002

# Include defaults if available
if [ -f /etc/default/$NAME ] ; then
        . /etc/default/$NAME
fi

if [ ! -d "$PIDFILE_DIR" ];then
        mkdir -p "$PIDFILE_DIR"
    chown $USER:$GROUP "$PIDFILE_DIR"
fi


case "$1" in
  start)
    echo "Starting $NAME"
    start-stop-daemon --start --background --pidfile $PIDFILE --exec $DAEMON -- -c $CONFIG -p $PIDFILE
		echo "."
    ;;
  stop)
    echo "Stopping $NAME"
    start-stop-daemon --stop --pidfile $PIDFILE --oknodo --exec $DAEMON --
    rm -f /var/run/turnserver/turnserver.pid
		echo "."
      ;;
  restart|force-reload)
    echo "Restarting $NAME"
    start-stop-daemon --stop --pidfile $PIDFILE --oknodo --exec $DAEMON --
    rm -f /var/run/turnserver/turnserver.pid
    sleep 2
    start-stop-daemon --start --background -m --pidfile $PIDFILE --exec $DAEMON -- -c $CONFIG
		echo "."
    ;;
  *)
    echo "Usage: /etc/init.d/$NAME {start|stop|restart}"
    exit 1
    ;;
esac

exit 0

